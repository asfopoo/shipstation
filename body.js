var prompt = require('prompt');
var request = require('request');
var shipstationAPI = require('node-shipstation');

var carriersList = [];
var shippingRatesList = new Array();


prompt.start();
var shipstation = new shipstationAPI(
    '0776d5b91fb4492a94ec443841b01fa3',
    '8e789c001268479cb6b781b9e09afc57');

function ShipstationWrapper(opts){
    const self = this;
    const {
        apiKey,
        apiSecret,
    } = opts;

    self.shipstation = new shipstationAPI(apiKey, apiSecret);

    self.getCarriers = self.getCarriers.bind(self);
    self.getShippingRates = self.getShippingRates.bind(self);
}

ShipstationWrapper.prototype = { //attaches a promise to all functions with a call back
    getCarriers(params){
        const self = this;
        return new Promise((resolve, reject) => {
            self.shipstation.getCarriers(params, (err, res, body) => {
               if(err){
                   return reject(err)
               }

               return resolve(body);
            });
        });
    },
    getShippingRates(params){
        const self = this;
        return new Promise((resolve, reject) => {
            self.shipstation.getShippingRates(params, (err, res, body) => {
                if(err){
                    return reject(err)
                }

                return resolve(body);
            });
        });
    },
    addShippingLabel(params){
        const self = this;
        return new Promise((resolve, reject) => {
            self.shipstation.addShippingLabel(params, (err, res, body) => {
                if(err){
                    return reject(err)
                }

                return resolve(body);
            });
        });
    },

};

const shipstationWrapper = new ShipstationWrapper({
    apiKey: '0776d5b91fb4492a94ec443841b01fa3',
    apiSecret:'8e789c001268479cb6b781b9e09afc57',
});


shipstationWrapper.getCarriers([]).then(carriers => {
    const carrierReq = [];
    carriers.forEach(carrier => {
       carrierReq.push({
           "carrierCode": carrier.code,
           "packageCode": 'package',
           "confirmation": 'delivery',
           "shipDate": '2018-05-28',
           "fromPostalCode": "78703",
           "toCountry": 'US',
           "toPostalCode": "20500",
           "weight": {
               value: 5,
               units: 'pounds'
           },
           "toCity": "washington"
       })
    });

    return Promise.resolve(carrierReq);
}).then(rateRequests => {
    const ratePromises = [];
    rateRequests.forEach(rateRequest => {
        ratePromises.push(shipstationWrapper.getShippingRates(rateRequest).then(results => {
            const updatedResults = results.map(result => {
                return Object.assign(result, {
                    carrierCode: rateRequest.carrierCode,
                });
            });
            return Promise.resolve(updatedResults);
        }));

    });
    return Promise.all(ratePromises);

}).then(rates => {
    const concatRates = [];
    rates.forEach(function(rate){
        //console.log(rate);
        concatRates.push.apply(concatRates, rate); //concats multiple arrays together
    });
    //console.log(concatRates);
    return Promise.resolve(concatRates);

}).then(shippingRatesList => {
    //console.log(shippingRatesList);
    if (!shippingRatesList || shippingRatesList.length < 1) {
        return Promise.reject(new Error("No Shipping Rates")); // if there are no rates
    }
    var lowestPrice = shippingRatesList[0].shipmentCost + shippingRatesList[0].otherCost;
    var lowestIndex = 0;
    for (let k = 1; k < shippingRatesList.length; k++) {      // reduces the list to min value
        if (shippingRatesList[k].shipmentCost + shippingRatesList[k].otherCost < lowestPrice) {
            lowestPrice = shippingRatesList[k].shipmentCost + shippingRatesList[k].otherCost;
            lowestIndex = k;
            //console.log("lowest1");
            //console.log(lowestIndex);
            //console.log("lowest1");
        }
    }
    //console.log(carriersList);
    //console.log(lowestIndex);
    //console.log("lowest2");
    return Promise.resolve(shippingRatesList[lowestIndex]);

}).then(createShippingLabel => {
    //console.log(createShippingLabel);

    const labelPromise = {
            "carrierCode": createShippingLabel.carrierCode,
            "serviceCode": createShippingLabel.serviceCode,
            "packageCode": "package",
            "confirmation": "delivery",
            "shipDate": '2018-05-29',
            "weight": {
                "value": 3,
                "units": "ounces"
            },
            "dimensions": {
                "units": "inches",
                "length": 7,
                "width": 5,
                "height": 6
            },
            "shipFrom": {
                "name": "Jason Hodges",
                "company": "ShipStation",
                "street1": "2815 Exposition Blvd",
                "street2": "Ste 2353242",
                "street3": null,
                "city": "Austin",
                "state": "TX",
                "postalCode": "78703",
                "country": "US",
                "phone": 7177414754,
                "residential": false
            },
            "shipTo": {
                "name": "The President",
                "company": "US Govt",
                "street1": "1600 Pennsylvania Ave",
                "street2": "Oval Office",
                "street3": null,
                "city": "Washington",
                "state": "DC",
                "postalCode": "20500",
                "country": "US",
                "phone": null,
                "residential": false
            },
            "insuranceOptions": null,
            "internationalOptions": null,
            "advancedOptions": null,
            "testLabel": false,
    };

    var neu = shipstationWrapper.addShippingLabel(labelPromise);
    return Promise.resolve(neu);

}).then(result => {
    console.log(result);
}).catch(err => {
    console.log("ERROR");
    console.log(err);
});













 /*  else if(result.selection == 2){
            console.log("your number two");

            var label = {
                "carrierCode": "fedex",
                "serviceCode": "fedex_ground",
                "packageCode": "package",
                "confirmation": "delivery",
                "shipDate": '2018-05-28',
                "weight": {
                    "value": 3,
                    "units": "ounces"
                },
                "dimensions": {
                    "units": "inches",
                    "length": 7,
                    "width": 5,
                    "height": 6
                },
                "shipFrom": {
                    "name": "Jason Hodges",
                    "company": "ShipStation",
                    "street1": "2815 Exposition Blvd",
                    "street2": "Ste 2353242",
                    "street3": null,
                    "city": "Austin",
                    "state": "TX",
                    "postalCode": "78703",
                    "country": "US",
                    "phone": 7177414754,
                    "residential": false
                },
                "shipTo": {
                    "name": "The President",
                    "company": "US Govt",
                    "street1": "1600 Pennsylvania Ave",
                    "street2": "Oval Office",
                    "street3": null,
                    "city": "Washington",
                    "state": "DC",
                    "postalCode": "20500",
                    "country": "US",
                    "phone": null,
                    "residential": false
                },
                "insuranceOptions": null,
                "internationalOptions": null,
                "advancedOptions": null,
                "testLabel": false,
            }


            shipstation.addShippingLabel(label, function(err, res, body){
                console.log(body);
            });

    }
});

*/






